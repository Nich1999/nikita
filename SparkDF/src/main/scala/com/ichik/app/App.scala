package com.ichik.app

import com.ichik.app.App.Config
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{asc, coalesce, col, desc, expr, lag, last, regexp_replace, row_number, substring, to_timestamp}

import java.io.{File, FileNotFoundException}
import scala.language.postfixOps
import scala.util.matching.Regex

/**
 * @author ${user.name}
 */
object App extends App{

  case class Config(url: String, username: String, password: String,
                    pathFiles: String, pathDBCreationQueries: String, pathTablesCreationQueries: String,
                    pathInsertOCQuery: String, pathInsertCCQuery: String,
                    pathInsertOCavgs: String, pathInsertCCavgs: String)

  def getArgs: (String, String, Boolean) = {
    if (args.length < 4) {
      throw new IllegalArgumentException("Error with args. You need enter: path to config file (-c); algorithm type: c-c/o-c (-a); init stagings (-d).")
    } else {
      var pathConfig = ""
      var algorithm = ""
      var initDatabase = false

      args.sliding(2).toList.collect {
        case Array(flag: String, argConfig: String) if (flag == "-c" || flag == "--configs") => pathConfig = argConfig
        case Array(flag: String, argAlg: String) if (flag == "-a" || flag == "--algorithm") => algorithm = argAlg
        case Array(flag: String, flag2: String) if (flag == "-d" || flag2 == "-d") => initDatabase = true
      }

            (pathConfig, algorithm, initDatabase)
    }
  }

  def getFilesPaths(filesPath: String, extractYearReg: Regex): Map[String, List[Map[String, List[String]]]] = {
    val source = new File(filesPath)
    val tools = source.listFiles.filter(_.isDirectory).map(_.getName).toList
    val selectMethod = raw"(?<=Secs_)(\w{3})"r
    val result = for (tool <- tools) yield {
      val files = new File(filesPath + "/" + tool).listFiles.map(_.toString).filter(_.endsWith(".csv")).toList
      val methodFiles = for (file <- files) yield
        (
          selectMethod.findFirstIn(file).mkString,
          file
        )
      val groupedMethods = methodFiles.groupBy(_._1).mapValues(_.map(_._2))
      (tool, groupedMethods)
    }
    result
      .groupBy(_._1)
      .mapValues(_.map(_._2))
      .mapValues(_.map(_.mapValues(_.sortWith(extractYearReg.findFirstIn(_).get < extractYearReg.findFirstIn(_).get))))
  }

  def getConfigs(pathConfigFile: String): Config = {
    try {
      val config = ConfigFactory.parseFile(new File(pathConfigFile))
      Config(
        config.getString("db.url"),
        config.getString("db.username"),
        config.getString("db.password"),
        config.getString("files.path_data_files"),
        config.getString("files.path_db_creation_queries"),
        config.getString("files.path_dst_creation_queries"),
        config.getString("files.path_insert_algorithm_o_c"),
        config.getString("files.path_insert_algorithm_c_c"),
        config.getString("files.path_insert_avgs_o_c"),
        config.getString("files.path_insert_avgs_c_c")
      )
    } catch {
      case e: FileNotFoundException => throw new FileNotFoundException(s"Config file not found. Path: ${pathConfigFile}")
    }
  }

  def getSparkSession: SparkSession = {
    Logger.getLogger("org").setLevel((Level.ERROR))
    SparkSession.builder()
      .master("local[*]").appName("Spark data frame")
      .getOrCreate()
  }

  def getCutted(
                 sparkSession: SparkSession,
                 path: String,
                 onlyClose: Boolean = false,
                 previousClose: Option[Float] = None
               ): DataFrame = {

    val dataFile = sparkSession.read
      .format("csv")
      .option("delimiter", ";")
      .option("header", "true")
      .load(path)

    val windowSpecMonth = Window.partitionBy("month").orderBy(col("month"))
    val windowForLast = Window.partitionBy("month")
    val windowForPreviousRow = Window.orderBy("month")

    val extractedMonths = dataFile
      .withColumn("month", substring(col("Time (UTC)"), 6, 2).cast("int")).as("Time")

    val cuttedRows = if (onlyClose) {
      extractedMonths
        .withColumn("row_number", row_number.over(windowSpecMonth))
        .filter("row_number = 1")
    } else {
      extractedMonths
        .withColumn("row_number", row_number.over(windowSpecMonth))
        .withColumn("last", last(col("row_number")).over(windowForLast))
        .filter("row_number = last OR row_number = 1")
    }

    val selectedColumns = cuttedRows
      .withColumn("open", regexp_replace(col("Open"), ",", ".").cast("float"))
      .withColumn("close", regexp_replace(col("Close"), ",", ".").cast("float"))
      .select(
        col("open"),
        col("close"),
        col("month")
      )

    if (onlyClose) {
      val joinedPrevious = previousClose match {
        case Some(previous) => selectedColumns
          .withColumn(
            "start",
            lag("close", 1, previous).over(windowForPreviousRow)
          )
        case None => selectedColumns
          .withColumn(
            "start",
            coalesce(
              lag("close", 1).over(windowForPreviousRow),
              col("open")
            )
          )
      }
      joinedPrevious
        .select(
          col("month"),
          col("start"),
          col("close")
        )
    } else {
      val numberedSelectedColumns = selectedColumns
        .withColumn("row_number", row_number.over(windowSpecMonth))

      numberedSelectedColumns
        .select(
          col("month"),
          col("close"),
          col("row_number")
        )
        .as("sc1")
        .join(
          numberedSelectedColumns
            .select(
              col("month"),
              col("open"),
              col("row_number")
            )
            .as("sc2"),
            col("sc1.month") === col("sc2.month") && col("sc1.row_number") === col("sc2.row_number") + 1,
          "inner"
        )
        .select(
          col("sc1.month").as("month"),
          col("open").as("start"),
          col("close")
        )
        .orderBy(col("month"))
    }
  }

  def main(): Unit = {

//    val (configPath, algorithm, initDatabase) = getArgs
    val configPath = "/home/mykytaichyk/BitBucket/nikita/etl.conf"
    val config = getConfigs(configPath)
    val extractYear = raw"(?<=_)(\d{4})"r

    val filesPaths = getFilesPaths(config.pathFiles, extractYear)

    val sparkSession = getSparkSession

    val pathFile = "/home/mykytaichyk/DataSet/USA500IDX/USA500IDXUSD_10 Secs_Bid_2012.01.16_2012.12.31.csv"

    val selectedColumns = getCutted(sparkSession, pathFile, onlyClose = false)

    selectedColumns.printSchema()
    selectedColumns.show(25)

    println("Hello World")
  }

  main()

}
