INSERT INTO money.avgs_o_c (tool, alg, jan, feb, mar, apr, may, june, july, aug, sept, `oct`, `nov`, `dec`, total)
SELECT
	tool,
	alg,
	AVG(jan),
	AVG(feb),
	AVG(mar),
	AVG(apr),
	AVG(may),
	AVG(june),
	AVG(july),
	AVG(aug),
	AVG(sept),
	AVG(`oct`),
	AVG(`nov`),
	AVG(`dec`),
	AVG(total)
FROM money.course_o_c
GROUP BY tool, alg;
