INSERT INTO money.course_c_c (tool, alg, `year`, jan, feb, mar, apr, may, june, july, aug, sept, `oct`, nov, `dec`, total)
WITH
	numbered_by_partition AS
	(
		SELECT *,
			ROW_NUMBER() OVER (PARTITION BY tool, alg ORDER BY `year`, `month`) AS cnt
		FROM stg_db.stg_course_grouped 
	),
	first_rows_by_partiotion AS
	(
		SELECT
			tool,
			alg,
			`year`,
			`month`,
			`start open` AS `start_close`,
			`finish close` AS `finish_close`
		FROM numbered_by_partition
		WHERE cnt = 1
	),
	joined_previous AS
	(
		SELECT * 
		FROM first_rows_by_partiotion
		UNION ALL
		SELECT 
			nbp.tool AS tool,
			nbp.alg AS alg,
			nbp.`year` AS `year`,
			nbp.`month` AS `month`,
			nbpp.`finish close` AS `start_close`,
			nbp.`finish close` AS `finish_close`
		FROM numbered_by_partition nbp
		INNER JOIN numbered_by_partition nbpp
		ON nbpp.cnt = nbp.cnt - 1 AND nbpp.tool = nbp.tool AND nbpp.alg = nbp.alg
	),
	select_first_last_months AS
    (
    	SELECT 
      		tool,
      		alg,
      		year,
      		MAX(month) AS last_month,
      		MIN(month) AS first_month
      	FROM
      		joined_previous
      	GROUP BY tool, alg, year
    ),
    totals AS 
    (
      SELECT
        sfl.tool,
        sfl.alg,
        sfl.year,
        (100 - ((jpc.finish_close * 100) / jp.start_close)) AS total
      FROM select_first_last_months sfl
      JOIN joined_previous jp
      ON sfl.tool = jp.tool AND sfl.alg = jp.alg AND sfl.year = jp.year AND sfl.first_month = jp.month
      JOIN joined_previous jpc
      ON sfl.tool = jpc.tool AND sfl.alg = jpc.alg AND sfl.year = jpc.year AND sfl.last_month = jpc.month
    ),
    counted_percents AS
	(
		SELECT
			tool,
			alg,
			`year`,
			`month`,
			(100 - ((finish_close * 100) / start_close)) AS percents
		FROM joined_previous
	),
	exponed_months AS
	(
		SELECT
    		tool,
    		alg,
    		`year`,
    		MAX(CASE WHEN (`month` = 1) THEN percents ELSE NULL END) AS jan,
    		MAX(CASE WHEN (`month` = 2) THEN percents ELSE NULL END) AS feb,
    		MAX(CASE WHEN (`month` = 3) THEN percents ELSE NULL END) AS mar,
    		MAX(CASE WHEN (`month` = 4) THEN percents ELSE NULL END) AS apr,
    		MAX(CASE WHEN (`month` = 5) THEN percents ELSE NULL END) AS may,
    		MAX(CASE WHEN (`month` = 6) THEN percents ELSE NULL END) AS june,
    		MAX(CASE WHEN (`month` = 7) THEN percents ELSE NULL END) AS july,
    		MAX(CASE WHEN (`month` = 8) THEN percents ELSE NULL END) AS aug,
    		MAX(CASE WHEN (`month` = 9) THEN percents ELSE NULL END) AS sept,
    		MAX(CASE WHEN (`month` = 10) THEN percents ELSE NULL END) AS `oct`,
    		MAX(CASE WHEN (`month` = 11) THEN percents ELSE NULL END) AS nov,
    		MAX(CASE WHEN (`month` = 12) THEN percents ELSE NULL END) AS `dec`
    	FROM counted_percents
    	GROUP BY tool, alg, `year`
	),
	joined_results AS 
	(
		SELECT
			em.*,
			t.total
		FROM exponed_months em
		JOIN totals t
		ON em.tool = t.tool AND em.alg = t.alg AND em.`year` = t.`year`
	)
SELECT * FROM joined_results;
