INSERT INTO stg_db.stg_course_grouped
  WITH marked AS
      (
          SELECT *,
              ROW_NUMBER() OVER
              (
                PARTITION BY tool, YEAR(`Time (UTC)`), MONTH(`Time (UTC)`)
                ORDER BY tool, alg, `Time (UTC)`
              ) AS `start/final`
          FROM stg_db.stg_course
      ),
      started AS
      (
          SELECT tool, alg, `Time (UTC)`, open, close
          FROM marked
          WHERE `start/final` = 1
      ),
      ended AS
      (
          SELECT tool, alg, `Time (UTC)`, open, close
          FROM marked
          WHERE `start/final` = 2
      ),
      joined AS
      (
          SELECT 
              o.tool AS tool, 
              o.alg AS alg, 
              YEAR(o.`Time (UTC)`) AS year,
              MONTH(o.`Time (UTC)`) AS month,
              o.open AS `start open`,
              o.close AS `start close`,
              c.open AS `finish open`,
              c.close AS `finish close`
          FROM started o
          INNER JOIN ended c
          ON o.tool = c.tool 
              AND o.alg = c.alg 
              AND YEAR(o.`Time (UTC)`) = YEAR(c.`Time (UTC)`)
              AND MONTH(o.`Time (UTC)`) = MONTH(c.`Time (UTC)`)
      )
  SELECT * FROM joined;
