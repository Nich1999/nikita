INSERT INTO money.course_o_c (tool, alg, `year`, jan, feb, mar, apr, may, june, july, aug, sept, `oct`, nov, `dec`, total)
WITH
	counted_data AS 
    (
    	SELECT 
            tool,
            alg,
            year,
            month,
            (100 - ((`finish close` * 100) / `start open`)) AS percents
        FROM
            stg_db.stg_course_grouped
    ),
    select_first_last_months AS
    (
    	SELECT 
      		tool,
      		alg,
      		year,
      		MAX(month) AS last_month,
      		MIN(month) AS first_month
      	FROM
      		stg_db.stg_course_grouped
      	GROUP BY tool, alg, year
    ),
    select_first_last_record AS 
    (
      SELECT
        sfl.tool,
        sfl.alg,
        sfl.year,
        (
          SELECT MAX(`start open`) 
          FROM stg_db.stg_course_grouped gr 
          WHERE sfl.tool = gr.tool AND sfl.alg = gr.alg AND sfl.year = gr.year AND sfl.first_month = gr.month
        ) AS open_year,
        (
          SELECT MAX(`finish close`) 
          FROM stg_db.stg_course_grouped gr 
          WHERE sfl.tool = gr.tool AND sfl.alg = gr.alg AND sfl.year = gr.year AND sfl.last_month = gr.month
        ) AS close_year
      FROM select_first_last_months sfl
    ),
    exponed_months AS
    (
    	SELECT
    		tool,
    		alg,
    		`year`,
    		MAX(CASE WHEN (`month` = 1) THEN percents ELSE NULL END) AS jan,
    		MAX(CASE WHEN (`month` = 2) THEN percents ELSE NULL END) AS feb,
    		MAX(CASE WHEN (`month` = 3) THEN percents ELSE NULL END) AS mar,
    		MAX(CASE WHEN (`month` = 4) THEN percents ELSE NULL END) AS apr,
    		MAX(CASE WHEN (`month` = 5) THEN percents ELSE NULL END) AS may,
    		MAX(CASE WHEN (`month` = 6) THEN percents ELSE NULL END) AS june,
    		MAX(CASE WHEN (`month` = 7) THEN percents ELSE NULL END) AS july,
    		MAX(CASE WHEN (`month` = 8) THEN percents ELSE NULL END) AS aug,
    		MAX(CASE WHEN (`month` = 9) THEN percents ELSE NULL END) AS sept,
    		MAX(CASE WHEN (`month` = 10) THEN percents ELSE NULL END) AS `oct`,
    		MAX(CASE WHEN (`month` = 11) THEN percents ELSE NULL END) AS nov,
    		MAX(CASE WHEN (`month` = 12) THEN percents ELSE NULL END) AS `dec`
    	FROM counted_data
    	GROUP BY tool, alg, `year`
    ),
    joined_results AS
    (
    	SELECT
	    	em.*,
	    	(100 - ((sfl.close_year * 100) / sfl.open_year)) AS total
	   	FROM exponed_months em
	   	INNER JOIN select_first_last_record sfl
	   	ON em.tool = sfl.tool AND em.alg = sfl.alg AND em.`year` = sfl.`year`
    )
SELECT * 
FROM joined_results;
