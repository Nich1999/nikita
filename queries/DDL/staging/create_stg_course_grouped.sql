CREATE TABLE stg_db.stg_course_grouped
(
	tool VARCHAR(20),
  	alg VARCHAR(3),
  	year INT,
  	month INT,
  	`start open` DECIMAL(7,3),
  	`start close` DECIMAL(7,3),
  	`finish open` DECIMAL(7,3),
  	`finish close` DECIMAL(7,3),
  	PRIMARY KEY (tool, alg, year, month)
);
