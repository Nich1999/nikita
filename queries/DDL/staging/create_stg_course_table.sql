CREATE TABLE stg_db.stg_course
(
  tool VARCHAR(20),
  alg  VARCHAR(3),
  `Time (UTC)` TIMESTAMP,
  open DECIMAL(7,3),
  close DECIMAL(7,3),
  PRIMARY KEY (tool, alg, `Time (UTC)`)
);
