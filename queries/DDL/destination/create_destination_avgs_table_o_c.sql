CREATE TABLE money.avgs_o_c
(
tool VARCHAR(25) NOT NULL,
alg VARCHAR(3) NOT NULL,
jan DECIMAL(4, 2),
feb DECIMAL(4, 2),
mar DECIMAL(4, 2),
apr DECIMAL(4, 2),
may DECIMAL(4, 2),
june DECIMAL(4, 2),
july DECIMAL(4, 2),
aug DECIMAL(4, 2),
sept DECIMAL(4, 2),
oct DECIMAL(4, 2),
nov DECIMAL(4, 2),
`dec` DECIMAL(4, 2),
total DECIMAL(4, 2),
PRIMARY KEY (tool, alg)
);
