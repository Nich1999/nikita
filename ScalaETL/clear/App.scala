import java.io.{File, FileNotFoundException}
import scala.collection.immutable.ListMap
import scala.io.Source
import scala.language.postfixOps
import scala.util.matching.Regex
import java.util.logging
import java.sql.{Connection, DriverManager, SQLException, Statement, Types}
import com.typesafe.config.ConfigFactory

import scala.collection.mutable.ArrayBuffer


/**
 * @author ${user.name}
 */
object App extends App {

  case class Row(month: Int, open: Float, close: Float)
  case class YearState(yearNumber: Int, months: Map[Int, (Row, Row)])

  val getDiff = (leftValue: Float, rightValue: Float) => 100 - ((rightValue * 100 ) / leftValue)

  /** Get args from console
   *  @return   tuple with params (path to files, path to requests, path to config, algorithm: o-c / c-c).
   */
  def getArgs: (String, String) = {
    if (args.length < 4) {
      throw new IllegalArgumentException("Error with args. You need enter: path to files (-f); path to requests (-r); path to config file (-c); algorithm type: c-c/o-c (-a)")
    } else {
      var pathConfig = ""
      var algorithm = ""

      args.sliding(2, 2).toList.collect {
        case Array(flag: String, argConfig: String) if (flag == "-c" || flag == "--configs") => pathConfig = argConfig
        case Array(flag: String, argAlg: String) if (flag == "-a" || flag == "--algorithm") => algorithm = argAlg
      }

      (pathConfig, algorithm)
    }
  }

  /** Extract data from one record from dataset.
   *  @param line      row with data from one record.
   *  @param monthReg  regular expression to extract month from date string.
   *  @return          class Row with data from one record (month, open, close).
   */
  def parseLine(line: String, monthReg: Regex): Row = {
    Row.tupled(
      line.replace(',', '.').split(';') match {
        case Array(
        month, open, _, _, close, _
        ) => (
          monthReg.findFirstIn(month).mkString.toInt, open.toFloat, close.toFloat
        )
      }
    )
  }

  /** Read data from single file.
   *  @param filePath    tuple with year and path to file.
   *  @param monthReg    regular expression to extract month from full date.
   *  @return            class YearState with fileds (year, list with months)
   */
  def readDataFromFile(filePath: (Int, String), monthReg: Regex): YearState = {
    val source = Source.fromFile(filePath._2)
    val rows = source
      .getLines()
      .drop(1)
      .map(parseLine(_, monthReg))
      .toList
      .groupBy(_.month)
      .map {
        case (month, list) => ( month -> (list.head, list.last))
      }
    YearState(filePath._1, ListMap(rows.toSeq.sortBy(_._1):_*))
  }

  /** Read data from multiple files.
   *  @param filePath    tuple with year and path to file.
   *  @param yearReg     regular expression to extract year from file name.
   *  @param monthReg    regular expression to extract month from full date.
   *  @return            array with YearState.
   */
  def readFiles(filePath: String,
                yearReg: Regex = raw"(?<=Bid_)(\d{4})"r,
                monthReg: Regex = raw"(?<=-)(\d{2})".r): Array[YearState] = {
    val files = new File(filePath).listFiles.map(_.toString).filter(_.endsWith(".csv"))

    val pathsYears = for (file <- files)
      yield {
        val year = yearReg.findFirstIn(file).mkString.toInt
        (year, file)
      }

    val sortedPaths = pathsYears.sortWith(_._1 < _._1)

    try {
      for (file <- sortedPaths)
        yield readDataFromFile(file, monthReg)
    } catch {
      case exception: FileNotFoundException => throw new FileNotFoundException(s"While reading files, exception: ${exception.getMessage}")
      case _: Throwable => throw new Exception("Exception while reading files.")
    }
  }

  /** Get statistics about one year.
   *  @param algorithmType    string with type of algorithm: o-c / c-c.
   *  @param yearState        YearState to calculate statistics.
   *  @param previousClose    close value from previous month, if first iterration = 0.
   *  @return                 tuple with stats about one year.
   */
  def getYearStatistic(algorithmType: String, yearState: YearState, previousClose: Float): (Int, Float, ArrayBuffer[Float]) = {
    val lastMonth = yearState.months.last._2._2
    val firstMonth = yearState.months.head._2._1
    var diffs = ArrayBuffer[Float]()

    algorithmType match {
      case "o-c" => {
        for (month <- yearState.months) {
          val open = month._2._1.open
          val close = month._2._2.close
            diffs += getDiff(open, close)
        }
        val total = getDiff(firstMonth.open, lastMonth.close)
        (yearState.yearNumber, total, diffs)
      }
      case "c-c" => {
        var prevClose = previousClose
        for (month <- yearState.months) {
          diffs += getDiff(prevClose, month._2._2.close)
          prevClose = month._2._2.close
        }
        prevClose = previousClose
        val total = getDiff(prevClose, lastMonth.close)
        (yearState.yearNumber, total, diffs)
      }
    }

  }

  /** Get statistics about all years.
   *  @param rows            array with data from files.
   *  @param algorithmType   string with type of algorithm: o-c / c-c.
   *  @return                array with tuples with statistics for every year in dataset.
   */
  def getAllYearsStats(rows: Array[YearState], algorithmType: String): Array[(Int, Float, ArrayBuffer[Float])] = {
    var previousClose = rows(0).months.head._2._1.open
    for (year <- rows)
      yield {
        val statisticYear = getYearStatistic(algorithmType, year, previousClose)
        previousClose = year.months.last._2._2.close
        statisticYear
      }
  }

  /** Calculate avg growing to every month.
   *  @param years    array with statistics to every year.
   *  @return         array with avg values.
   */
  def getAvgByMonth(years: Array[(Int, Float, ArrayBuffer[Float])]): ArrayBuffer[Float] = {
    val sumPerMonth = new Array[Float](12)
    val counts = new Array[Int](12)
    val res = new ArrayBuffer[Float]()
    for {
      year <- years
    } {
      val months = year._3
      for (i <- 0 to 11) {
        try {
          sumPerMonth(i) += months(i)
          counts(i) += 1
        } catch {
          case e: IndexOutOfBoundsException => None
        }
      }

    }
    sumPerMonth.map(_ / years.length)
    for ((sum, count) <- sumPerMonth zip counts) {
      res += sum / count
    }

    res
  }

  /** Calculate avg totals growing.
   *  @param years    array with statistics to every year.
   *  @return         avg totals values.
   */
  def getAvgTotals(years: Array[(Int, Float, ArrayBuffer[Float])]): Float = {
    val totals = for (year <- years) yield year._2
    totals.sum / years.length
  }

  /** Get configuation data from config file
   *  @param pathConfigFile path to config file
   *  @return               tuple with values: urlDB, username, password, path to data files, path to queries files
   */
  def getConfigs(pathConfigFile: String): (String, String, String, String, String) = {
    try {
      val config = ConfigFactory.parseFile(new File(pathConfigFile))
      (
        config.getString("db.url"),
        config.getString("db.username"),
        config.getString("db.password"),
        config.getString("files.path_data_files"),
        config.getString("files.path_requests_files")
      )
    } catch {
      case e: FileNotFoundException => throw new FileNotFoundException(s"Config file not found. Path: ${pathConfigFile}")
    }
  }

  /** Get connection to database
   * @param url      url of database
   * @param username username for db user
   * @param password password for db user
   * @return         connection for mysql database
   */
  def getConnectionDB(url: String, username: String, password: String): Connection = {
    try {
      Class.forName("com.mysql.cj.jdbc.Driver")
      DriverManager.getConnection(url, username, password)
    } catch {
      case e: Exception => throw new InterruptedException(s"Error with connection. Error: ${e.getMessage}")
    }
  }

  /** Extract query from file
   * @param pathQuery path to folder with query files
   * @return          query from sql file
   */
  def getQueryFromFile(pathQuery: String): String = {
    try {
      val source = Source.fromFile(pathQuery)
      val queryText = source.getLines().mkString(" ")
      source.close()
      queryText
    } catch {
      case e: FileNotFoundException => throw new FileNotFoundException(s"Error with query file. Error: ${e.getMessage}")
    }
  }

  /** Execute all queries in one folder
   * @param queries   array with queries
   * @param statement statement to execute queries
   */
  def executeAllInFolder(queries: Array[String], statement: Statement): Unit = {
    try {
      for (query <- queries) {
        val queryText = getQueryFromFile(query)
        statement.executeUpdate(queryText)
      }
    } catch {
      case e: FileNotFoundException => throw new FileNotFoundException(s"Didn't found query. Error: ${e.getMessage}")
      case e: SQLException => throw new SQLException(s"Error is sql syntax. Error: ${e.getMessage}")
    }

  }

  /** Execute all queries that creates databases / tables
   * @param pathQueries path to files with queries
   * @param connection  connection to database
   */
  def executeCreationQueries(pathQueries: String,
                     connection: Connection): Unit = {
    try {
      val createDBQueries = new File(pathQueries + "/databases").listFiles.map(_.toString).filter(_.endsWith(".sql"))
      val createTablesQueries = new File(pathQueries + "/tables").listFiles.map(_.toString).filter(_.endsWith(".sql"))
      val statement = connection.createStatement()
      executeAllInFolder(createDBQueries, statement)
      executeAllInFolder(createTablesQueries, statement)
    } catch {
      case e: FileNotFoundException => throw new FileNotFoundException(s"Files with queries not found. Error: ${e.getMessage}")
      case e: SQLException => throw new IllegalArgumentException(s"Error in sql syntax. Error: ${e.getMessage}")
      case e: Exception => throw new IllegalArgumentException(s"Error in creation queries. Error: ${e.getMessage}")
    }
  }

  /** Execute all queries to insert data to database
   * @param pathQueries path to files with queries
   * @param connection  connection to database
   * @param statistics  array with statistics for every month
   * @param avgs        array with averages
   * @param avgTotals   average from all totals values
   */
  def executeInsertQueries(pathQueries: String,
                           connection: Connection,
                           statistics: Array[(Int, Float, ArrayBuffer[Float])],
                           avgs: ArrayBuffer[Float],
                           avgTotals: Float): Unit = {
    try {
      val queryMainInsert = getQueryFromFile(pathQueries + "/insert/insertMain.sql")
      val queryAvgsInsert = getQueryFromFile(pathQueries + "/insert/insertAvgs.sql")
      val preparedStatementForMainStats = connection.prepareStatement(queryMainInsert)
      val preparedStatementForAvgs = connection.prepareStatement(queryAvgsInsert)
      for (stat <- statistics) {
        preparedStatementForMainStats.setInt(1, stat._1)
        for (i <- 2 to 13) {
          try {
            preparedStatementForMainStats.setFloat(i, stat._3(i - 2))
          } catch {
            case e: IndexOutOfBoundsException => preparedStatementForMainStats.setNull(i, Types.NULL);
          }
        }
        preparedStatementForMainStats.setFloat(14, stat._2)
        preparedStatementForMainStats.executeUpdate()
      }
      for (i <- 1 to 12 ) {
        try {
          preparedStatementForAvgs.setFloat(i, avgs(i - 1))
        } catch {
          case e: IndexOutOfBoundsException => preparedStatementForAvgs.setFloat(i, Types.NULL)
        }
      }
      preparedStatementForAvgs.setFloat(13, avgTotals)
      preparedStatementForAvgs.executeUpdate()
    } catch {
      case e: SQLException => throw new IllegalArgumentException(s"Error in SQL. Error: ${e.getMessage}")
    }
  }

  def main: Unit = {

    val (pathConfig, algorithm) = getArgs

    val (url, username, password, dataPath, requestsPath) = getConfigs(pathConfig)

    val data = readFiles(dataPath)
    val connectionDB = getConnectionDB(url, username, password)

    val statistics = getAllYearsStats(data, algorithm)
    val avgs = getAvgByMonth(statistics)
    val avgTotals = getAvgTotals(statistics)

    executeCreationQueries(requestsPath, connectionDB)
    executeInsertQueries(requestsPath, connectionDB, statistics, avgs, avgTotals)
    connectionDB.close()
  }

  main

}


