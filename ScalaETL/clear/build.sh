
#Default variables
pathToBuild=""

#Read params from console
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -p | --path_build)  pathToBuild="$2"; shift ;;
        *) echo "Error with parameters: $1" >&2; exit 1 ;;
    esac
    shift
done

#Commands

mkdir $pathToBuild/ScalaETL
cp -R lib $pathToBuild/ScalaETL

scalac -cp "$(printf %s: lib/*.jar)" -d $pathToBuild/ScalaETL/run.jar App.scala
printf 'scala -cp "$(printf %%s: lib/*.jar)" run.jar $1 $2 $3 $4' > $pathToBuild/ScalaETL/run.sh
