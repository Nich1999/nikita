package com.ichik.app

import com.fasterxml.jackson.core.async.NonBlockingInputFeeder
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark._
import org.apache.log4j._
import org.apache.spark.rdd.RDD

import java.io.{File, FileNotFoundException}
import java.sql.{Connection, DriverManager, SQLException, Statement, Types}
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.language.postfixOps
import scala.util.matching.Regex


object App extends App {

  case class Config(url: String, username: String, password: String,
                    pathFiles: String, pathDBCreationQueries: String, pathTablesCreationQueries: String,
                    pathInsertOCQuery: String, pathInsertCCQuery: String,
                    pathInsertOCavgs: String, pathInsertCCavgs: String)

  case class DstRecord(tool: String, alg: String, year: Int, months: Array[Float], total: Int)

  val getDiff = (leftValue: Float, rightValue: Float) => 100 - ((rightValue * 100 ) / leftValue)

  def getArgs: (String, String, Boolean) = {
    if (args.length < 4) {
      throw new IllegalArgumentException("Error with args. You need enter: path to config file (-c); algorithm type: c-c/o-c (-a); init stagings (-d).")
    } else {
      var pathConfig = ""
      var algorithm = ""
      var initDatabase = false

      args.sliding(2).toList.collect {
        case Array(flag: String, argConfig: String) if (flag == "-c" || flag == "--configs") => pathConfig = argConfig
        case Array(flag: String, argAlg: String) if (flag == "-a" || flag == "--algorithm") => algorithm = argAlg
        case Array(flag: String, flag2: String) if (flag == "-d" || flag2 == "-d") => initDatabase = true
      }

      (pathConfig, algorithm, initDatabase)
    }
  }

  def getFilesPaths(filesPath: String, extractYearReg: Regex): Map[String, List[Map[String, List[String]]]] = {
    val source = new File(filesPath)
    val tools = source.listFiles.filter(_.isDirectory).map(_.getName).toList
    val selectMethod = raw"(?<=Secs_)(\w{3})"r
    val result = for (tool <- tools) yield {
      val files = new File(filesPath + "/" + tool).listFiles.map(_.toString).filter(_.endsWith(".csv")).toList
      val methodFiles = for (file <- files) yield
        (
          selectMethod.findFirstIn(file).mkString,
          file
        )
      val groupedMethods = methodFiles.groupBy(_._1).mapValues(_.map(_._2))
      (tool, groupedMethods)
    }
    result
      .groupBy(_._1)
      .mapValues(_.map(_._2))
      .mapValues(_.map(_.mapValues(_.sortWith(extractYearReg.findFirstIn(_).get < extractYearReg.findFirstIn(_).get))))
  }

  def getConfigs(pathConfigFile: String): Config = {
    try {
      val config = ConfigFactory.parseFile(new File(pathConfigFile))
      Config(
        config.getString("db.url"),
        config.getString("db.username"),
        config.getString("db.password"),
        config.getString("files.path_data_files"),
        config.getString("files.path_db_creation_queries"),
        config.getString("files.path_dst_creation_queries"),
        config.getString("files.path_insert_algorithm_o_c"),
        config.getString("files.path_insert_algorithm_c_c"),
        config.getString("files.path_insert_avgs_o_c"),
        config.getString("files.path_insert_avgs_c_c")
      )
    } catch {
      case e: FileNotFoundException => throw new FileNotFoundException(s"Config file not found. Path: ${pathConfigFile}")
    }
  }

  def calculateYearStatistics(data: Array[(Int, (Float, Float))]): (Float, Array[Float]) = {
    (
      getDiff(data(0)._2._1, data.last._2._2),
      for (record <- data) yield getDiff(record._2._1, record._2._2)
    )
  }

  def getAvgByMonth(years: List[(Int, Float, Array[Float])]): ArrayBuffer[Float] = {
    val sumPerMonth = new Array[Float](12)
    val counts = new Array[Int](12)
    val res = new ArrayBuffer[Float]()
    for {
      year <- years
    } {
      val months = year._3
      for (i <- 0 to 11) {
        try {
          sumPerMonth(i) += months(i)
          counts(i) += 1
        } catch {
          case e: IndexOutOfBoundsException => None
        }
      }

    }
    sumPerMonth.map(_ / years.length)
    for ((sum, count) <- sumPerMonth zip counts) {
      res += sum / count
    }

    res
  }

  def getAvgTotals(years: List[(Int, Float, Array[Float])]): Float = {
    val totals = for (year <- years) yield year._2
    totals.sum / years.length
  }

  def calculateAndInsertStats(sparkContext: SparkContext,
                              filesPaths: Map[String, List[Map[String, List[String]]]],
                              algType: String,
                              extractYear: Regex,
                              config: Config,
                              connection: Connection): Unit = {

    for (
      (tool, values) <- filesPaths;
      value <- values
    ) {
      for ((alg, paths) <- value) {
        var previousClose = None: Option[Float]
        val yearsStatistics = for (path <- paths) yield {
          val year = extractYear.findFirstIn(path).get
          val records = algType match {
            case "o-c" => readDataFromFile(sparkContext, path)
            case "c-c" => readDataFromFile(sparkContext, path, previousClose, onlyClose = true)
            case _ => throw new IllegalArgumentException("Entered wrong algorithm of calculation stats.")
          }
          previousClose = Some(records.last._2._2)
          val yearStatistics = calculateYearStatistics(records)
          (year.toInt, yearStatistics._1, yearStatistics._2)
        }
        val avgsByEveryMonth = getAvgByMonth(yearsStatistics)
        val avgToTals = getAvgTotals(yearsStatistics)
        executeInsertQueries(tool, alg, config, connection, algType, yearsStatistics, avgsByEveryMonth, avgToTals)
      }
    }


  }

  def readDataFromFile(sparkContext: SparkContext,
                       pathFile: String,
                       previousClose: Option[Float] = None,
                       onlyClose: Boolean = false): Array[(Int, (Float, Float))] = {

    def splitRow(row: String): (Int, (Float, Float)) = {
      val columns = row.split(";")
      (
        columns(0).slice(5, 7).toInt,
        (
          columns(1).replace(",", ".").toFloat,
          columns(4).replace(",", ".").toFloat
        )
      )
    }

    def getMonthAndIndex(record: (Long, String), coefficient: Int): (Long, String) = {
      (
        record._1 + coefficient,
        record._2.slice(5, 7)
      )
    }

    def filterBorderRecordsPerMonth(record: (Long, (String, Option[String]))): Boolean = {
      val monthToCompare = record._2._2
      val monthInRecord = record._2._1.slice(5, 7)

      monthToCompare match {
        case Some(month) => !monthInRecord.equals(month)
        case None => true
      }
    }

    def joinNumbersOfMonthsAndFiltering(left: RDD[(Long, String)], right: RDD[(Long, String)]):RDD[String] = {
      left
      .leftOuterJoin(right)
      .filter(filterBorderRecordsPerMonth)
      .map(_._2._1)
    }

    val rowsWithoutHeader = sparkContext.textFile(pathFile)
      .filter(!_.startsWith("T"))
      .zipWithIndex
      .map(_.swap)

    val previousMonths = rowsWithoutHeader.map(getMonthAndIndex(_, -1))
    val lastRecords = joinNumbersOfMonthsAndFiltering(rowsWithoutHeader, previousMonths)
      .map(splitRow)
      .mapValues(_._2)

    if (onlyClose) {
      val firstRecord = splitRow(rowsWithoutHeader.first()._2)
      val preprocessedFirstRecord = previousClose match {
        case Some(close) => (firstRecord._1, close)
        case None => (firstRecord._1, firstRecord._2._1)
      }
      val allCloses = preprocessedFirstRecord +: lastRecords.collect().sortBy(_._1)
      val groupedCloses = for (i <- 0 until allCloses.length - 1) yield {
        val tempMonth = allCloses(i)
        val nextMonth = allCloses(i + 1)
        (i + 1, (tempMonth._2, nextMonth._2))
      }
      groupedCloses.toArray
    } else {
      val forewardMonths = rowsWithoutHeader.map(getMonthAndIndex(_, 1))
      val firstRecords = joinNumbersOfMonthsAndFiltering(rowsWithoutHeader, forewardMonths)
      firstRecords
        .map(splitRow)
        .mapValues(_._1)
        .join(lastRecords)
        .sortByKey()
        .collect()
    }

  }

  def getConnectionDB(config: Config): Connection = {
    try {
      Class.forName("com.mysql.cj.jdbc.Driver")
      DriverManager.getConnection(config.url + "?rewriteBatchedStatements=true", config.username, config.password)
    } catch {
      case e: Exception => throw new InterruptedException(s"Error with connection. Error: ${e.getMessage}")
    }
  }

  def getQueryFromFile(pathQuery: String): String = {
    try {
      val source = Source.fromFile(pathQuery)
      val queryText = source.getLines().mkString(" ")
      source.close()
      queryText
    } catch {
      case e: FileNotFoundException => throw new FileNotFoundException(s"Error with query file. Error: ${e.getMessage}")
    }
  }

  def executeAllInFolder(queries: Array[String], statement: Statement): Unit = {
    try {
      for (query <- queries) {
        val queryText = getQueryFromFile(query)
        statement.executeUpdate(queryText)
      }
    } catch {
      case e: FileNotFoundException => throw new FileNotFoundException(s"Didn't found query. Error: ${e.getMessage}")
      case e: SQLException => throw new SQLException(s"Error is sql syntax. Error: ${e.getMessage}")
    }

  }

  def executeCreationQueries(config: Config,
                             connection: Connection): Unit = {
    try {
      val createDBQueries = new File(config.pathDBCreationQueries).listFiles.map(_.toString).filter(_.endsWith(".sql"))
      val createTablesQueries = new File(config.pathTablesCreationQueries).listFiles.map(_.toString).filter(_.endsWith(".sql"))
      val statement = connection.createStatement()
      executeAllInFolder(createDBQueries, statement)
      executeAllInFolder(createTablesQueries, statement)
    } catch {
      case e: FileNotFoundException => throw new FileNotFoundException(s"Files with queries not found. Error: ${e.getMessage}")
      case e: SQLException => throw new IllegalArgumentException(s"Error in sql syntax. Error: ${e.getMessage}")
      case e: Exception => throw new IllegalArgumentException(s"Error in creation queries. Error: ${e.getMessage}")
    }
  }

  def executeInsertQueries(tool: String,
                           alg: String,
                           config: Config,
                           connection: Connection,
                           algorithmCounting: String,
                           statistics: List[(Int, Float, Array[Float])],
                           avgs: ArrayBuffer[Float],
                           avgTotals: Float): Unit = {
    try {
      var queryMainInsert = ""
      var queryAvgsInsert = ""
      algorithmCounting match {
        case "o-c" => {
          queryMainInsert = getQueryFromFile(config.pathInsertOCQuery)
          queryAvgsInsert = getQueryFromFile(config.pathInsertOCavgs)
        }
        case "c-c" => {
          queryMainInsert = getQueryFromFile(config.pathInsertCCQuery)
          queryAvgsInsert = getQueryFromFile(config.pathInsertCCavgs)
        }
      }
      val preparedStatementForMainStats = connection.prepareStatement(queryMainInsert)
      val preparedStatementForAvgs = connection.prepareStatement(queryAvgsInsert)
      for (stat <- statistics) {
        preparedStatementForMainStats.setString(1, tool)
        preparedStatementForMainStats.setString(2, alg)
        preparedStatementForMainStats.setInt(3, stat._1)
        for (i <- 4 to 15) {
          try {
            preparedStatementForMainStats.setFloat(i, stat._3(i - 4))
          } catch {
            case e: IndexOutOfBoundsException => preparedStatementForMainStats.setNull(i, Types.NULL);
          }
        }
        preparedStatementForMainStats.setFloat(16, stat._2)
        preparedStatementForMainStats.executeUpdate()
      }
      preparedStatementForAvgs.setString(1, tool)
      preparedStatementForAvgs.setString(2, alg)
      for (i <- 3 to 14) {
        try {
          preparedStatementForAvgs.setFloat(i, avgs(i - 3))
        } catch {
          case e: IndexOutOfBoundsException => preparedStatementForAvgs.setFloat(i, Types.NULL)
        }
      }
      preparedStatementForAvgs.setFloat(15, avgTotals)
      preparedStatementForAvgs.executeUpdate()
    } catch {
      case e: SQLException => throw new IllegalArgumentException(s"Error in SQL. Error: ${e.getMessage}")
    }
  }

  def main(): Unit = {

    Logger.getLogger("org").setLevel((Level.ERROR))
    val sc = new SparkContext("local[*]", "SparkRDDETL")

    val (configPath, algorithm, initDatabase) = getArgs
    val config = getConfigs(configPath)
    val extractYear = raw"(?<=_)(\d{4})"r
    val connectionDB = getConnectionDB(config)

    val filesPaths = getFilesPaths(config.pathFiles, extractYear)

    if (initDatabase) {
      executeCreationQueries(config, connectionDB)
    }

    calculateAndInsertStats(sc, filesPaths, algorithm, extractYear, config, connectionDB)

    connectionDB.close()
  }

  main()

}
