pathToBuild=""

#Read params from console
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -p | --path_build)  pathToBuild="$2"; shift ;;
        *) echo "Error with parameters: $1" >&2; exit 1 ;;
    esac
    shift
done

#Commands
mkdir $pathToBuild/JavaETL
mkdir build
mkdir build/META-INF
cp -R lib build
cp -R lib $pathToBuild/JavaETL
javac -d build -cp .:./lib/* $(find ./src/com/ichik/app/ -name '*.java')
( cd build ; printf "Manifest-Version: 1.0\nMain-Class: com.ichik.app.App\nClass-Path: lib/commons-cli-1.4.jar lib/config-1.4.0.jar lib/mysql-connector-java-8.0.26.jar\n" > MANIFEST.MF)
( cd build ; jar cfm archive.jar MANIFEST.MF com lib)
( cd build ; mv archive.jar $pathToBuild/JavaETL/run.jar )
rm -r build


