package com.ichik.app;

public class Row {

    private int monthNumber;
    private float open;
    private float close;

    public Row(int monthNumber,
               float open,
               float close) {
        this.monthNumber = monthNumber;
        this.open = open;
        this.close = close;
    }

    public int getMonthNumber() {
        return monthNumber;
    }

    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public float getOpen() {
        return open;
    }

    public void setOpen(float open) {
        this.open = open;
    }

    public float getClose() {
        return close;
    }

    public void setClose(float close) {
        this.close = close;
    }

    @Override
    public String toString() {
        return "Row{" +
                "monthNumber=" + monthNumber +
                ", open=" + open +
                ", close=" + close +
                '}';
    }
}
