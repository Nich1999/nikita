package com.ichik.app.exceptions;

public class ConfigETLException extends Exception {
    public ConfigETLException(String errorMessage) {
        super(errorMessage);
    }
}
