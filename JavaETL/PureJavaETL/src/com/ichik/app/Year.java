package com.ichik.app;

import java.util.List;

public class Year {
    private int yearNumber;
    private List<Row> rows;

    public Year(int yearNumber, List<Row> rows) {
        this.yearNumber = yearNumber;
        this.rows = rows;
    }

    public int getYearNumber() {
        return yearNumber;
    }

    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }

    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    @Override
    public String toString() {
        return "Year{" +
                "yearNumber=" + yearNumber +
                ", months=" + rows.toString() +
                "}\n";
    }
}
