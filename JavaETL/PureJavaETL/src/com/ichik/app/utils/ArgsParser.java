package com.ichik.app.utils;
import org.apache.commons.cli.*;

public class ArgsParser {

    public static Options getOptions() {
        Options options = new Options();

        Option pathData = new Option("c", "config", true, "Path to file with configs.");
        pathData.setRequired(true);
        options.addOption(pathData);

        Option algorithmType = new Option("a", "algorithm", true, "Type of algorithm.");
        algorithmType.setRequired(true);
        options.addOption(algorithmType);

        return options;
    }

    public static CommandLine getParser(Options options, String[] args) throws ParseException, Exception {
        CommandLineParser parser = new DefaultParser();
        try {
            return parser.parse(options, args);
        } catch (ParseException e) {
            throw new ParseException(String.format("Error while parse args from console, Exception: %s", e.getMessage()));
        } catch (Exception e) {
            throw new Exception(String.format("Error while parsing the arguments. Error: %s", e.getMessage()));
        }
    }
}
