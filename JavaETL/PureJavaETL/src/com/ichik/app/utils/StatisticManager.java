package com.ichik.app.utils;

import com.ichik.app.Row;
import com.ichik.app.Year;

import java.util.*;
import java.util.stream.Collectors;

public class StatisticManager {

    public static TreeMap<Integer, ArrayList<Float>> getStatistics(ArrayList<Year> years, String typeAlgo) throws IllegalArgumentException{

        final float[] recentClose = {years.get(0).getRows().get(0).getOpen()};
        TreeMap<Integer, ArrayList<Float>> results = new TreeMap<>();

        for (Year year : years) {
            ArrayList<Float> statisticsByYear = new ArrayList<>();
            float total = 0.0f;
            Map<Integer, List<Row>> grouped = year.getRows()
                    .stream()
                    .collect(Collectors
                            .groupingBy(Row::getMonthNumber));
            grouped.forEach((key, value) -> {
                float open = value.get(0).getOpen();
                float close = value.get(1).getClose();

                if (typeAlgo.equals("o-c")) {
                    statisticsByYear.add(getDiff(open, close));
                } else if (typeAlgo.equals("c-c")){
                    statisticsByYear.add(getDiff(recentClose[0], close));
                    recentClose[0] = close;
                } else {
                    throw new IllegalArgumentException(String.format("Error with type of algorithm. Algorithm: %s not supported.", typeAlgo));
                }
            });
            results.put(year.getYearNumber(), statisticsByYear);
        }
        return results;
    }

    public static TreeMap<Integer, Float> getTotals(ArrayList<Year> years, String typeAlgo) throws IllegalArgumentException {

        final float[] recentClose = {years.get(0).getRows().get(0).getOpen()};
        TreeMap<Integer, Float> results = new TreeMap<>();
        for (Year year : years) {
            List<Row> rows = year.getRows();
            float close = rows.get(rows.size() - 1).getClose();
            if (typeAlgo.equals("o-c")) {
                float open = rows.get(0).getOpen();
                results.put(year.getYearNumber(), getDiff(open, close));
            } else if (typeAlgo.equals("c-c")){
                results.put(year.getYearNumber(), getDiff(recentClose[0], close));
                recentClose[0] = close;
            } else {
                throw new IllegalArgumentException(String.format("Error with type of algorithm. Algorithm: %s not supported.", typeAlgo));
            }
        }
        return results;
    }

    public static float[] getAvgs(TreeMap<Integer, ArrayList<Float>> statistics) {

        float[] results = new float[12];
        float[] sums = new float[12];
        int[] counts = new int[12];
        statistics.forEach((key, value) -> {
            for (int i = 0; i < 12; i++) {
                try {
                    sums[i] += value.get(i);
                } catch (IndexOutOfBoundsException e) {
                    continue;
                }
                counts[i] += 1;
            }
        });
        for (int i = 0; i < 12; i++) {
            results[i] = sums[i] / counts[i];
        }
        return results;
    }

    public static float getMean(Map<Integer, Float> statistics) {
        final float[] sum = {0};
        statistics.forEach((key, value) -> {
            sum[0] += value;
        });
        return sum[0] / statistics.size();
    }

    private static float getDiff(float leftValue, float rightValue) {
        return 100 - ((rightValue * 100) / leftValue);
    }
}
