package com.ichik.app.utils;

import java.io.File;
import java.io.FileNotFoundException;

import com.ichik.app.exceptions.ConfigETLException;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;
import com.typesafe.config.ConfigFactory;

public class ConfigReader {
    public static Config readConfigFile(String pathConfigFile) throws ConfigETLException {
        try {
            File configFile = new File(pathConfigFile);
            return ConfigFactory.parseFile(configFile);
        } catch (NullPointerException e) {
            throw new ConfigETLException(String.format("Error, file not found. Error: %s", e.getMessage()));
        } catch (ConfigException e) {
            throw new ConfigETLException(String.format("Error with config file. Error: %s", e.getMessage()));
        }
    }
}
