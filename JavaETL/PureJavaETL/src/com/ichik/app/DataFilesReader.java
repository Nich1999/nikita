package com.ichik.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataFilesReader {

    private Pattern extractMonth;
    private Pattern extractYear;
    private Pattern extractYearFromName;

    public DataFilesReader() {
        this.extractMonth = Pattern.compile("(?<=-)(\\d{2})(?=-)");
        this.extractYear = Pattern.compile("(\\d{4})(?=-)");
        this.extractYearFromName = Pattern.compile("(?<=Bid_)(\\d{4})");
    }

    public DataFilesReader(String extractMonth,
                           String extractYear,
                           String extractYearFromName) {
        this.extractMonth = Pattern.compile(extractMonth);
        this.extractYear = Pattern.compile(extractYear);
        this.extractYearFromName = Pattern.compile(extractYearFromName);
    }

    public Year readFile(String pathFile) throws IOException {
        try {
            FileReader reader = new FileReader(pathFile);
            BufferedReader bufferedReader = new BufferedReader(reader);
            List<Row> months = new ArrayList<Row>();

            int year = 0;
            String recentLine = "";
            int recentMonth = 0;
            int tempMonth = 0;
            Matcher matcher = null;

            String line;
            bufferedReader.readLine();
            while ((line = bufferedReader.readLine()) != null) {
                try {
                    matcher = this.extractMonth.matcher(line);
                } catch (NullPointerException e) {
                    continue;
                }
                if (recentMonth == 0) {
                    Matcher yearMatcher = this.extractYear.matcher(line);
                    year = matchInt(yearMatcher);
                    recentMonth = matchInt(matcher);
                    Row parsedLine = parseLine(recentMonth, line);
                    months.add(parsedLine);
                } else {
                    tempMonth = recentMonth;
                    recentMonth = matchInt(matcher);
                    if (recentMonth != tempMonth) {
                        Row recentRow = parseLine(tempMonth, recentLine);
                        months.add(recentRow);
                        Row liveRow = parseLine(recentMonth, line);
                        months.add(liveRow);
                    }
                }
                recentLine = line;
            }
            reader.close();
            Row finalRow = parseLine(recentMonth, recentLine);
            months.add(finalRow);
            return new Year(year, months);

        } catch (IOException e) {
            throw new IOException(String.format("Error while reading file by path: %s. Error: %s", pathFile, e.getMessage()));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public ArrayList<Year> readFiles(String pathFolder) throws NullPointerException, IOException {
        try {

            String[] files = this.getSortedFilenames(pathFolder);

            ArrayList<Year> years = new ArrayList<>();

            for (String file : files) {
                Year year = readFile(file);
                years.add(year);
            }

            return years;

        } catch (NullPointerException e) {
            throw new NullPointerException(String.format("Null pointer exception while reading files. Error: %s", e.getMessage()));
        } catch (IOException e) {
            throw new IOException(String.format("IOException while reading files. Error: %s", e.getMessage()));
        }
    }

    private String[] getSortedFilenames(String pathFolder) throws NullPointerException {
        try {
            String[] files = Arrays.stream(
                            Objects.requireNonNull(
                                    new File(pathFolder)
                                            .listFiles()))
                    .map(File::toString)
                    .filter((str) -> str.endsWith(".csv")).toArray(String[]::new);

            Map<Integer, String> sortedMap = new TreeMap<>();
            Matcher matcher = null;
            for (String file : files) {
                matcher = this.extractYearFromName.matcher(file);
                int year = matchInt(matcher);
                sortedMap.put(year, file);
            }

            return sortedMap.values().toArray(new String[0]);

        } catch (NullPointerException e) {
            throw new NullPointerException(String.format("Null pointer exception while reading names of files. Error: %s", e.getMessage()));
        }
    }

    private Row parseLine(int numberMonth, String line) {
        String[] splittedLine = line.split(";");
        float open = Float.parseFloat(splittedLine[1].replace(",", "."));
        float close = Float.parseFloat(splittedLine[4].replace(",", "."));
        return new Row(numberMonth, open, close);
    }

    private int matchInt(Matcher matcher) {
        int result = 0;
        while (matcher.find()) {
            result = Integer.parseInt(matcher.group());
        }
        return result;
    }

    public Pattern getExtractYearFromName() {
        return extractYearFromName;
    }

    public void setExtractYearFromName(Pattern extractYearFromName) {
        this.extractYearFromName = extractYearFromName;
    }

    public Pattern getExtractMonth() {
        return extractMonth;
    }

    public void setExtractMonth(Pattern extractMonth) {
        this.extractMonth = extractMonth;
    }

    public Pattern getExtractYear() {
        return extractYear;
    }

    public void setExtractYear(Pattern extractYear) {
        this.extractYear = extractYear;
    }

}
