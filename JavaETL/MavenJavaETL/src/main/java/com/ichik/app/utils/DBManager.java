package com.ichik.app.utils;

import com.ichik.app.exceptions.DatabaseConnectException;
import com.typesafe.config.Config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

public class DBManager {

    private Connection connection;

    public DBManager(Config config) throws DatabaseConnectException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String connectionURL = config.getString("db.url");
            String username = config.getString("db.username");
            String password = config.getString("db.password");

            this.connection = DriverManager.getConnection(connectionURL, username, password);
        } catch (ClassNotFoundException e) {
            throw new DatabaseConnectException(String.format("Error with connecting to DB. Error: %s", e.getException().getMessage()));
        } catch (SQLException e) {
            throw new DatabaseConnectException(String.format("Error with connection to DB. Error: %s", e.getMessage()));
        }
    }

    private String getQueryFromFile(String pathFile) throws FileNotFoundException, IOException {
        try {
            return String.join(" ", Files.readAllLines(Paths.get(pathFile)));
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException(String.format("Error while file not found. Error: %s", e.getMessage()));
        } catch (IOException e) {
            throw new IOException(String.format("Error while reading query from file. Error: %s", e.getMessage()));
        }
    }

    private void executeAllInFolder(String[] queries, Statement statement) throws FileNotFoundException, SQLException, IOException {
        try {
            for (String query : queries) {
                String sqlQuerie = getQueryFromFile(query);
                statement.executeUpdate(sqlQuerie);
            }
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException(String.format("Error while file not found. Error: %s", e.getMessage()));
        } catch (SQLException e) {
            throw new SQLException(String.format("Error while executing query. Error: %s", e.getMessage()));
        } catch (IOException e) {
            throw new IOException(String.format("Error while executing query (IO error). Error: %s", e.getMessage()));
        }
    }

    public void executeCreationQueries(String pathQueries) throws FileNotFoundException, SQLException, IOException, NullPointerException {
        try {
            String[] createDBQueries = getFilesInFolder(pathQueries + "/databases");
            String[] createTablesQueries = getFilesInFolder(pathQueries + "/tables");
            Statement statement = connection.createStatement();
            executeAllInFolder(createDBQueries, statement);
            executeAllInFolder(createTablesQueries, statement);
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException(String.format("Error while executing creation scripts. Error: %s", e.getMessage()));
        } catch (SQLException e) {
            throw new SQLException(String.format("Error while executing creation scripts. Error: %s", e.getMessage()));
        } catch (IOException e) {
            throw new IOException(String.format("Error while executing creation scripts. Error: %s", e.getMessage()));
        } catch (NullPointerException e) {
            throw new NullPointerException(String.format("Error while executing creation scripts. Error: %s", e.getMessage()));
        }
    }

    public void executeInsertQueries(String pathQueries,
                                     Map<Integer, ArrayList<Float>> statistics,
                                     float[] avgs,
                                     Map<Integer, Float> totals,
                                     float avgTotals) throws FileNotFoundException, SQLException, IOException, NullPointerException {
        try {
            Map<Integer, ArrayList<Float>> preprocessedStatistics = statistics;
            String mainInsertQuery = getQueryFromFile(pathQueries + "/insert/insertMain.sql");
            String avgsInsertQuery = getQueryFromFile(pathQueries + "/insert/insertAvgs.sql");
            PreparedStatement preparedStatementMainStats = this.connection.prepareStatement(mainInsertQuery);
            PreparedStatement preparedStatementAvgs = this.connection.prepareStatement(avgsInsertQuery);

//            preprocessedStatistics.forEach((key, value) -> {
//                int length = value.size();
//                while (length < 12) {
//                    value.add(0f);
//                    length += 1;
//                }
//            });

            preprocessedStatistics.forEach((key, value) -> {

                try {
                    preparedStatementMainStats.setInt(1, key);
                    for (int i = 2; i < 14; i++) {
                        try {
                            preparedStatementMainStats.setFloat(i, value.get(i - 2));
                        } catch (IndexOutOfBoundsException e) {
                            preparedStatementMainStats.setNull(i, Types.NULL);
                        }

                    }
                    preparedStatementMainStats.setFloat(14, totals.get(key));
                    preparedStatementMainStats.executeUpdate();
                } catch (SQLException e) {
                    throw new RuntimeException(String.format("Error in SQL syntax while inserting data. Error: %s", e.getMessage()));
                }

            });

            for (int i = 0; i < 12; i++) {
                try {
                    preparedStatementAvgs.setFloat(i + 1, avgs[i]);
                } catch (IndexOutOfBoundsException e) {
                    preparedStatementAvgs.setNull(i + 1, Types.NULL);
                }

            }
            preparedStatementAvgs.setFloat(13, avgTotals);
            preparedStatementAvgs.executeUpdate();

        } catch (FileNotFoundException e) {
            throw new FileNotFoundException(String.format("Error while executing creation scripts. Error: %s", e.getMessage()));
        } catch (SQLException e) {
            throw new SQLException(String.format("Error while executing creation scripts. Error: %s", e.getMessage()));
        } catch (IOException e) {
            throw new IOException(String.format("Error while executing creation scripts. Error: %s", e.getMessage()));
        } catch (NullPointerException e) {
            throw new NullPointerException(String.format("Error while executing creation scripts. Error: %s", e.getMessage()));
        }
    }

    private String[] getFilesInFolder(String pathFiles) throws NullPointerException {
        try {
            return Arrays.stream(
                            Objects.requireNonNull(
                                    new File(pathFiles)
                                            .listFiles()))
                    .map(File::toString)
                    .filter((str) -> str.endsWith(".sql")).toArray(String[]::new);
        } catch (NullPointerException e) {
            throw new NullPointerException(String.format("Error while executing creation scripts. Error: %s", e.getMessage()));
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }


}
