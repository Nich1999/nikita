package com.ichik.app.exceptions;

public class DatabaseConnectException extends Exception{
    public DatabaseConnectException(String errorMessage) {
        super(errorMessage);
    }
}
