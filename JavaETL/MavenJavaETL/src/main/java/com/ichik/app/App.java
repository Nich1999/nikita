package com.ichik.app;
import com.ichik.app.utils.ArgsParser;
import com.ichik.app.utils.ConfigReader;
import com.ichik.app.utils.DBManager;
import com.ichik.app.utils.StatisticManager;
import com.typesafe.config.Config;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;

import java.util.ArrayList;
import java.util.TreeMap;

public class App 
{
    public static void main( String[] args ) throws Exception {
        Options optionsToParse = ArgsParser.getOptions();
        try {

            CommandLine commandLine = ArgsParser.getParser(optionsToParse, args);
            String pathConfig = commandLine.getOptionValue("config");
            String algorithmType = commandLine.getOptionValue("algorithm");
            Config config = ConfigReader.readConfigFile(pathConfig);

            DBManager databaseManager = new DBManager(config);
            DataFilesReader reader = new DataFilesReader();

            String pathFiles = config.getString("files.path_data_files");
            String pathQueries = config.getString("files.path_requests_files");

            ArrayList<Year> years = reader.readFiles(pathFiles);

            TreeMap<Integer, ArrayList<Float>> statistics = StatisticManager.getStatistics(years, algorithmType);
            TreeMap<Integer, Float> totals = StatisticManager.getTotals(years, algorithmType);
            float[] avgs = StatisticManager.getAvgs(statistics);
            float avgTotal = StatisticManager.getMean(totals);


            databaseManager.executeCreationQueries(pathQueries);
            databaseManager.executeInsertQueries(pathQueries, statistics, avgs, totals, avgTotal);

        } catch (Exception e) {
            System.exit(1);
        }
    }
}
