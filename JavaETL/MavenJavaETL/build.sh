#Default variables
pathToBuild=""

#Read params from console
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -p | --path_build)  pathToBuild="$2"; shift ;;
        *) echo "Error with parameters: $1" >&2; exit 1 ;;
    esac
    shift
done

#Commands


mvn clean compile assembly:single
( cd target ; mv JavaEtl-1.0-SNAPSHOT-jar-with-dependencies.jar $pathToBuild/javaETL.jar ;  )
