package com.ichik.app
import java.io.{File, FileNotFoundException}
import scala.io.Source
import java.sql.{Connection, DriverManager, SQLException, Statement, Timestamp, Types}
import com.typesafe.config.ConfigFactory

import scala.language.postfixOps
import java.util.Scanner
import scala.collection.mutable.ArrayBuffer

/**
 * @author ${user.name}
 */
object App {

  case class Config(url: String, username: String, password: String,
                    pathFiles: String, pathInsertStg: String,
                    pathInsertGroupedStg: String, pathDSTTablesCreateQueries: String,
                    pathSTGTablesCreateQueries: String, pathDBCreationQueries: String,
                    pathInsertAlgOC: String, pathInsertAlgCC: String,
                    pathInsertAvgsOC: String, pathInsertAvgsCC: String)
  
  def getArgs(args: Array[String]): (String, String, Boolean) = {
    if (args.length < 4) {
      throw new IllegalArgumentException("Error with args. You need enter: path to config file (-c); algorithm type: c-c/o-c (-a); init stagings (-d).")
    } else {
      var pathConfig = ""
      var algorithm = ""
      var initStagings = false

      args.sliding(2).toList.collect {
        case Array(flag: String, argConfig: String) if (flag == "-c" || flag == "--configs") => pathConfig = argConfig
        case Array(flag: String, argAlg: String) if (flag == "-a" || flag == "--algorithm") => algorithm = argAlg
        case Array(flag: String, flag2: String) if (flag == "-d" || flag2 == "-d") => initStagings = true
      }

      (pathConfig, algorithm, initStagings)
    }
  }

  def getConfigs(pathConfigFile: String): Config = {
    try {
      val config = ConfigFactory.parseFile(new File(pathConfigFile))
      Config(
        config.getString("db.url"),
        config.getString("db.username"),
        config.getString("db.password"),
        config.getString("files.path_data_files"),
        config.getString("files.path_insert_to_stg_query"),
        config.getString("files.path_group_stg_insert"),
        config.getString("files.path_dst_creation_queries"),
        config.getString("files.path_stg_creation_queries"),
        config.getString("files.path_db_creation_queries"),
        config.getString("files.path_insert_stg_algorithm_o_c"),
        config.getString("files.path_insert_stg_algorithm_c_c"),
        config.getString("files.path_insert_dst_avgs_o_c"),
        config.getString("files.path_insert_dst_avgs_c_c")
      )
    } catch {
      case e: FileNotFoundException => throw new FileNotFoundException(s"Config file not found. Path: ${pathConfigFile}")
    }
  }

  def getFilesPaths(filesPath: String): Map[String, List[Map[String, List[String]]]] = {
    val source = new File(filesPath)
    val tools = source.listFiles.filter(_.isDirectory).map(_.getName).toList
    val selectMethod = raw"(?<=Secs_)(\w{3})"r
    val result = for (tool <- tools) yield {
      val files = new File(filesPath + "/" + tool).listFiles.map(_.toString).filter(_.endsWith(".csv")).toList
      val methodFiles = for (file <- files) yield
        (
          selectMethod.findFirstIn(file).mkString,
          file
        )
      val groupedMethods = methodFiles.groupBy(_._1).mapValues(_.map(_._2))
      (tool, groupedMethods)
    }
    result.groupBy(_._1).mapValues(_.map(_._2))
  }

  def getQuery(queryPath: String): String = {
    val source = Source.fromFile(queryPath)
    val query = source.getLines().toList.mkString(" ")
    source.close()
    query
  }

  def getConnection(config: Config): Connection = {
    try {
      Class.forName("com.mysql.cj.jdbc.Driver")
      DriverManager.getConnection(config.url + "?rewriteBatchedStatements=true", config.username, config.password)
    } catch {
      case e: Exception => throw new InterruptedException(s"Error with connection. Error: ${e.getMessage}")
    }
  }

  def readFile(pathFile: String): ArrayBuffer[(String, String, String)] = {
    val extractMonth = raw"(?<=-)(\d{2})"r
      val scanner = new Scanner(new File(pathFile))
      val result = new ArrayBuffer[(String, String, String)]()

      val recentLine = Array("", "", "")
      val updateRecentLine = (date: String, open: String, close: String) => {
        recentLine(0) = date
        recentLine(1) = open
        recentLine(2) = close
      }
      var recentMonth = 0
      var tempMonth = 0

      scanner.nextLine()
      while (scanner.hasNext()) {
        val line = scanner.nextLine()
        val columns = line.split(";")
        val date = columns(0)
        val open = columns(1)
        val close = columns(4)
        if (recentMonth == 0) {
          recentMonth = extractMonth.findFirstIn(date).mkString.toInt
          result.append((date, open, close))
        } else {
          tempMonth = recentMonth
          recentMonth = extractMonth.findFirstIn(date).mkString.toInt
          if (recentMonth != tempMonth) {
            result.append((recentLine(0), recentLine(1), recentLine(2)))
            result.append((date, open, close))
          }
        }
        updateRecentLine(date, open, close)
      }
    result.append((recentLine(0), recentLine(1), recentLine(2)))
    result
  }

  def readFullFile(pathFile: String): ArrayBuffer[(String, String, String)] = {
    val scanner = new Scanner(new File(pathFile))
    val result = ArrayBuffer[(String, String, String)]()

    scanner.nextLine()
    while (scanner.hasNext()) {
      val line = scanner.nextLine()
      val columns = line.split(";")
      result.append((columns(0), columns(1), columns(4)))
    }
    result
  }

  def executeSingleQuery(query: String, connection: Connection): Unit = {
    val statement = connection.createStatement()
    try {
      statement.executeUpdate(query)
    } catch {
      case e: Exception => throw new InterruptedException(s"Error with executing query. Error: ${e.getMessage}")
    }
  }

  def executeInsertingDataToStagings(files: Map[String, List[Map[String, List[String]]]],
                                     pathInsertStg: String,
                                     connection: Connection,
                                     readFile: (String) => ArrayBuffer[(String, String, String)]): Unit = {
    val loadQuery = getQuery(pathInsertStg)
    val statement = connection.prepareStatement(loadQuery)
    for (
      (tool, values) <- files;
      value <- values;
      (alg, paths) <- value;
      path <- paths;
      record <- readFile(path)
    ) {
      statement.setString(1, tool)
      statement.setString(2, alg)
      statement.setTimestamp(3, Timestamp.valueOf(record._1))
      statement.setFloat(4, record._2.replace(",", ".").toFloat)
      statement.setFloat(5, record._3.replace(",", ".").toFloat)
      statement.addBatch()
    }
    statement.executeBatch()
  }

  def executeAllInFolder(queries: Array[String], statement: Statement): Unit = {
    for (queryPath <- queries) {
      val query = getQuery(queryPath)
      statement.executeUpdate(query)
    }
  }

  def executeDDL(pathDDLQueries: String,
                 connection: Connection): Unit = {
    val queries = new File(pathDDLQueries).listFiles.map(_.toString).filter(_.endsWith(".sql"))
    val statement = connection.createStatement()
    executeAllInFolder(queries, statement)
  }

  def initStagings(config: Config, connection: Connection): Unit = {
    val filesPaths = getFilesPaths(config.pathFiles)
    executeDDL(config.pathDBCreationQueries, connection)
    executeDDL(config.pathDSTTablesCreateQueries, connection)
    executeDDL(config.pathSTGTablesCreateQueries, connection)
    executeInsertingDataToStagings(filesPaths, config.pathInsertStg, connection, readFile)

    val insertGroupedQuery = getQuery(config.pathInsertGroupedStg)

    executeSingleQuery(insertGroupedQuery, connection)
  }

  def executeCountingAlgorithm(algorithmType: String, config: Config, connection: Connection): Unit = {
    algorithmType match {
      case "o-c" => {
        executeSingleQuery(getQuery(config.pathInsertAlgOC), connection)
        executeSingleQuery(getQuery(config.pathInsertAvgsOC), connection)
      }
      case "c-c" => {
        executeSingleQuery(getQuery(config.pathInsertAlgCC), connection)
        executeSingleQuery(getQuery(config.pathInsertAvgsCC), connection)
      }
      case _ => throw new IllegalArgumentException("Illegal algorithm")
    }

  }

  def main(args : Array[String]): Unit = {
    val (pathConfig, algorithm, initStagingsFlag) = getArgs(args)
    val config = getConfigs(pathConfig)

    val connection = getConnection(config)

    if (initStagingsFlag) {
      initStagings(config, connection)
    }

    executeCountingAlgorithm(algorithm, config, connection)
    connection.close()
  }

}
