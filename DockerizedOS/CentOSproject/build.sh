
#Default variables
nameContainer="centos_project_$$"
sshPrv="/dev/null"
sshPub="/dev/null"
gitUrl=""

#Read params from console
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -n | --name_container)  nameContainer="$2"; shift ;;
        -prv | --ssh_private_path)   sshPrv="$2"; shift ;;
        -pub | --ssh_public_path)    sshPub="$2"; shift ;;
        -url | --git_url)       gitUrl="$2"; shift ;;
        *) echo "Error with parameters: $1" >&2; exit 1 ;;
    esac
    shift
done

#Build command
docker build \
    -t $nameContainer \
    --build-arg sshPrv="$(cat ${sshPrv})" \
    --build-arg sshPub="$(cat ${sshPub})" \
    --build-arg gitUrl=$gitUrl .

exit 0